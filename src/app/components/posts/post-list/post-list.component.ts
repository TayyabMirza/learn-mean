import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
  // posts = [
  //   { title: 'First post', content: 'This is first post\'s content' },
  //   { title: 'Second post', content: 'This is second post\'s content' },
  //   { title: 'Third post', content: 'This is third post\'s content' },
  //   { title: 'Fourth post', content: 'This is fourth post\'s content' }
  // ]
  posts: Post[] = []
  private postsSub: Subscription;

  constructor(public postsService: PostsService) { }

  ngOnInit(): void {
    this.posts = this.postsService.getPosts();
    this.postsSub = this.postsService.getPOstUpdateListner().subscribe(
      (posts: Post[]) => {
        this.posts = posts
      }
    );
  }

  ngOnDestroy(): void {
    this.postsSub.unsubscribe();
  }

}
